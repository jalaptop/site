---
title: "Enter the space"
linktitle: "Enter the space"
---

**If one of the doors is closed, [give us a call](/contact).**

 # From Brussels Midi to Studio CityGate
{{< image src="/images/enter_space/001.png">}}

# Enter the Pink Gate and walk to Door A
{{< image src="/images/enter_space/002.png">}}

# Pink gate
{{< image src="/images/enter_space/003.jpg">}}

# Door A
{{< image src="/images/enter_space/004.jpg">}}

# First stairs
{{< image src="/images/enter_space/005.jpg">}}

# Second stairs
{{< image src="/images/enter_space/006.jpg">}}

# Third stairs
{{< image src="/images/enter_space/007.jpg">}}

# Follow the black arrows
{{< image src="/images/enter_space/008.jpg">}}
{{< image src="/images/enter_space/009.jpg">}}

# Finally there
{{< image src="/images/enter_space/010.jpg">}}
