---
eventid: "coremeeting14"
startdate:  2019-02-09
starttime: "13:00"
linktitle: "Coremeeting 14"
title: "Coremeeting 14"
location: "HSBXL"
eventtype: "Meeting"
price: ""
series: "coremeeting"
hackeragenda: "false"
---

Our monthly CoreMeeting. Discussing main issues like the bookkeeping.  
Exceptionally not on the first Saturday of the month because Bytenight.


# Notes

w, betz, aska @hs,  
nino, ptr, zoobab more or less remotely


## rent for month december issue

we got payment reminder from landlord (entrakt) for december -- we paid for january;
gerd gave oral ok to start paying from january, but apparently this never got taken in account at their end
we already payed (aska)
gerd points us to 'accounting@entrakt.be' > catch 22 because their accounting isn't aware of the oral agreement

end of november 2018 the oral agreement was made | the contract says 1 december though

Actionplan:
- get written confirmation of the oral agreement (e.g. email)
- forward it to bookkeeping accounting@entrakt.be

every Friday afternoon there's a sort of centralised complaining point with entrakt at the climbing wall place = at the other wing (entrance through the skatepark entrance Industrielaan, or though the elevator hall none obvious way thingie)

betz follows up? 

## warranty rue manchester
w will ask sarah (brussels gewest) -- as soon as paperwork is ready she'll reach out (last communication mid januari)
normally they have 3 months - should be end of march latest

## kbc account
w would take this (whole board to go to kbc)  
writing a letter (recommande) but first get the form
mail sent to bankswitching@kbc.be

making an appointment at kbc bxl city-center 12th of february 2019   in the evening 19th of february 2019

## membership fee
tom  didn't have time to make a draft, postponed
membership fee is 15 -- you're invited to pay more 25eur if you can  (we need around 20eur pp to break even)
Current monthly membership income (january 2019): 605€
Current rent amount: 662,36 €
--> 57,36 € to go for membership to cover 100% of the rent!

## rodents
- 2 catches so far
- are they connected to the network ? Not yet (SOON)
{{< youtube _mej5wS7viw >}}

## bookkeeping overview
- http://hsbxl.be/books  > not active yet
- Balance on 2019-01-01: ?€
- Balance on 2019-02-09: 4003.1200€
- coming costs:
  - feb rent  662,36 €
  - heating ? 1-2 bags p day >  5eur per day - 150 per month
  - warranty citygate (if the question arrises)  2 months of rent
- coming income:
- maybe recup december2018 rent ? 

## Byteweek aftermath
- https://hsbxl.be/events/byteweek/2019/#aftermath
- Byteweek brought us €2851,5 > revenue
- All pre-bytenight bills have been paid.
- 1274,19 profit 
- VGC bill for broken chairs still to pay : 26eur (paid)

## Club Mate
- is a mess : lovibond  (we asked for a pallet, reminder after 2 weeks, then betz  rushed for delivery -- and delivery got delayed last minute >> not serious)
- drinkcenter in leuven ? job buys there; w has contact -- https://www.bierenwijnhuis.be/frisdranken/123-club-mate-6x50cl.html
- dorstloescher aachen ?  last time they didnt want to deliver because of 'import issues'
- group-buy with kolab, climbing wall e.a. ?
  
  
## interactions with landlord 

- last monday 4th feb around noon gerd was in space with some electrician, without anyone present;   he notified s ok
- elec is fixed 
  - we have decent earth wire connection now
  - still issue with chained differentiels > can break serially and we don't have access to the one on the first floor
- hsbxl gives support for wifi ?  important that the board communicates in unison to the landlord  (both on support for wifi; camera; unexpected bills etc.)
  - requests in this sense : standard answer is ' we'll discuss it in the meeting'  > further contact: w's phone 
       
- there are camera's at each entrance
- w is going to install some extra camera's and will indicate them clearly
- should perhaps think about legal aspects
- leverage for better elec > if power goes down the camera footage might get lost
- w has a UPS, strategy undefined

       
## burned pellets bin needs to be metal or ceramik
- florist shops ?
- bend some metal shelf ?   
- 13 eur on bol.com  

## date next coremeeting
- 1st saturday of march  >> Saturday 2nd of march 2019
- make a habit of reminding members  before next coremeeting


