---
startdate:  2020-06-15
starttime: "9:00"
enddate: 2020-06-19
endtime: "17:00"
allday: true
linktitle: "Piggyback Ride 1"
title: "Piggyback Ride 1"
location: "HSBXL"
eventtype: "follow an online course"
price: "NULL"
image: "runningpig.jpg"
series: "Piggyback Ride"
start: "true"
#aliases: [/piggybackride]
---

# What ?
It's an online course gracefully offered by a member's employer.
We don't mind shoulder surfing: feel free to sit behind us and follow what is being said and done.
We will set up the space so you can confortably follow the stream with us.

# Subject
In case you're interested by the subject, it will be about F5 LTM and APM. Maybe a bit of ASM

# Remarks
Use of power/noisy tools is strongly discouraged. Please keep chatter to a minimum. No music will be played.
We won't necessarily have the time/resources to answer to questions: consider us as being at work.
A brief explanation can be done whenever possible, during breaks.

This is a new concept: some kinks need to be worked out.

# Who ?
- Askarel
- Habifo
