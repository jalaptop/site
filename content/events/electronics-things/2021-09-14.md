---
startdate:  2021-09-14
starttime: "19:00"
endtime: "20:30"
linktitle: "How I made my first PCB in KiCAD"
title: "How I made my first PCB in KiCAD"
price: "Free"
series: "electronics"
eventtype: "members"
location: "HSBXL"
image: "pcb.png"
hackeragenda: "true"
---

KiCAD is an open source application to develop schematics and designing the PCB.
Sharing my first experiences with the program and made a simple Arduino shield (works for Raspberry Pi hat too).
Let's share knowledge together.
