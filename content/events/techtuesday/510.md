---
eventid: techtue510
startdate:  2019-02-19
starttime: "19:00"
linktitle: "TechTue 510"
title: "TechTuesday 510"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
