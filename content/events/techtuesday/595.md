---
techtuenr: "595"
startdate: 2021-09-21
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue595
series: TechTuesday
title: TechTuesday 595
linktitle: "TechTue 595"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
