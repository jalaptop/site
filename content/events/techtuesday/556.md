---
eventid: techtue556
startdate:  2020-01-14
starttime: "19:00"
linktitle: "TechTue 556"
title: "TechTuesday 556"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
