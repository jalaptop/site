---
startdate:  2019-03-30
starttime: "14:00"
#enddate: 2019-02-02
#endtime: "05h"
allday: false
linktitle: "General Assembly"
title: "General Assembly 14"
location: "HSBXL"
eventtype: "General assembly"
price: "membership"
image: "ga.png"
series: "GA"
hackeragenda: "false"
---


# Present members
- Askarel
- Anita 
- Wouter
- Betz
- Ricardo
- Nicolas
- Olivier
- Jurgen
- Vincent
- lomo

# Acceptance books of last year
There are questions about where the expenses are going to. We had a couple of Colruyt runs, in December we also rented a van (€250) this was refunded this year.
It is possible to see a autogenerated bookkeeping summary on https://hsbxl.be/books/


# warranty rue manchester
- account was never tranferred with sell of building. still in name of abel falisse.
- wouter and aska will make a form for the bank for unlocking the account with argenta


# mail / space address
we have permission to put a mailbox at entrance of shittygate and put our address.


# asbestos
we had the floor and the glue under the floor tested by a lab, they don't contain asbestos.

# Cleanup moments
Wouter is going to kick off some "cleanup" moments.  
The starting idea is to have it a few hours before TechTue.  
Could be combined with a BBQ.  
People only showing up for BBQ ... meh, won't even mention what we're going to do with them...

# Account issues with Argenta bank
Our bank account has been flagged for a while because of not being in order with paperwork: the people on the statutes do not match with the papers for the bank.
As soon as the statutes are updated, we should also bring the paperwork with the bank in order.

# Statutes need to be updated
To change:  
- board members: Vincent, Tom, Fred, Wouter.  
- address  
- reference address


# Member count update
Counting list on query of March: 27 people paying €15 or more.  
(taken from the books page on HSBXL books page):

- 2019 (2 months): 1355€ (extrapolates to €8130)
- 2018: 7975€
- 2017: 8075€
- 2016: 8045€
- 2015: 7065.5€
- 2014: 5390€
    
# Membership page
Default phrasing: "minimum €15 but we need €25 to break even". 

# Club Mate
Wouter and Olivier will go on a road trip to Aachen to get Club Mate.  
Vincent will look to borrow a camionette from work.

# Bankaccount 

## 2017
    
    January      BalanceStart: 1885.5400    BalanceEnd: 2515.2300    Income: 629.6900     Spending: NULL         Trend: 629.6900    
    February     BalanceStart: 2515.2300    BalanceEnd: 3404.1100    Income: 1965.0000    Spending: -1076.1200   Trend: 888.8800    
    March        BalanceStart: 3404.1100    BalanceEnd: 2951.5400    Income: 1050.0000    Spending: -1502.5700   Trend: -452.5700   
    April        BalanceStart: 2951.5400    BalanceEnd: 3120.5400    Income: 1143.0000    Spending: -974.0000    Trend: 169.0000    
    May          BalanceStart: 3120.5400    BalanceEnd: 2963.0800    Income: 825.0000     Spending: -982.4600    Trend: -157.4600   
    June         BalanceStart: 2963.0800    BalanceEnd: 2839.0700    Income: 850.0000     Spending: -974.0100    Trend: -124.0100   
    July         BalanceStart: 2839.0700    BalanceEnd: 1635.1000    Income: 900.0000     Spending: -2103.9700   Trend: -1203.9700  
    August       BalanceStart: 1635.1000    BalanceEnd: 2683.1000    Income: 1048.0000    Spending: NULL         Trend: 1048.0000   
    September    BalanceStart: 2683.1000    BalanceEnd: 2437.2700    Income: 1016.0000    Spending: -1261.8300   Trend: -245.8300   
    October      BalanceStart: 2437.2700    BalanceEnd: 2775.3300    Income: 2238.0600    Spending: -1900.0000   Trend: 338.0600    
    November     BalanceStart: 2775.3300    BalanceEnd: 2882.5300    Income: 930.5000     Spending: -823.3000    Trend: 107.2000    
    December     BalanceStart: 2882.5300    BalanceEnd: 3190.0300    Income: 1207.5000    Spending: -900.0000    Trend: 307.5000    

## 2018

    January      BalanceStart: 3190.0300    BalanceEnd: 2955.0300    Income: 815.0000     Spending: -1050.0000   Trend: -235.0000   
    February     BalanceStart: 2955.0300    BalanceEnd: 3832.0100    Income: 2790.0000    Spending: -1913.0200   Trend: 876.9800    
    March        BalanceStart: 3832.0100    BalanceEnd: 4842.1400    Income: 1227.5000    Spending: -217.3700    Trend: 1010.1300   
    April        BalanceStart: 4842.1400    BalanceEnd: 4549.2300    Income: 1227.0900    Spending: -1520.0000   Trend: -292.9100   
    May          BalanceStart: 4549.2300    BalanceEnd: 4388.9500    Income: 960.2300     Spending: -1120.5100   Trend: -160.2800   
    June         BalanceStart: 4388.9500    BalanceEnd: 3710.2700    Income: 1223.0000    Spending: -1901.6800   Trend: -678.6800   
    July         BalanceStart: 3710.2700    BalanceEnd: 3540.2700    Income: 780.0000     Spending: -950.0000    Trend: -170.0000   
    August       BalanceStart: 3540.2700    BalanceEnd: 3890.2700    Income: 1300.0000    Spending: -950.0000    Trend: 350.0000    
    September    BalanceStart: 3890.2700    BalanceEnd: 4025.8900    Income: 1284.5400    Spending: -1148.9200   Trend: 135.6200    
    October      BalanceStart: 4025.8900    BalanceEnd: 2727.2900    Income: 972.5000     Spending: -2271.1000   Trend: -1298.6000  
    November     BalanceStart: 2727.2900    BalanceEnd: 3131.6100    Income: 1674.0000    Spending: -1269.6800   Trend: 404.3200    
    December     BalanceStart: 3131.6100    BalanceEnd: 3121.6100    Income: 840.0000     Spending: -850.0000    Trend: -10.0000    

## 2019
    
    January      BalanceStart: 3121.6100    BalanceEnd: 2245.4700    Income: 870.0000     Spending: -1746.1400   Trend: -876.1400   
    February     BalanceStart: 2245.4700    BalanceEnd: 4621.5000    Income: 3802.0000    Spending: -1425.9700   Trend: 2376.0300   
    March        BalanceStart: 4621.5000    BalanceEnd: 4039.3800    Income: 1143.8000    Spending: -1725.9200   Trend: -582.1200   


# Assembly :)
~~~~
SECTION .data		; data section
	msg:	db "GENRALSMBY ",11	; the string to print, 10=cr
	len:	equ $-msg		; "$" means "here"
				; len is a value, not an address

	SECTION .text		; code section
	        global _start		; make label available to linker
	_start:				; standard  gcc  entry point

	mov	eax,4		;
	mov	ecx,msg		;
	mov	edx,3		;
	mov	ebx,1		; write sysout command to int 80 hex
	int	0x80		; interrupt 80 hex, call kernel
	mov	eax,4       ;
	    mov esi,1           ;
	    add ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    mov esi,2           ;
	    add ecx,esi         ;
	    mov edx, 3
	    int 0x80
	mov	eax,4       ;
	    mov esi,7           ;
	    add ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    mov esi,6           ;
	    sub ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    mov esi,2           ;
	    add ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    int 0x80
	mov	eax,4       ;
	    mov esi,5           ;
	    sub ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    mov esi,6           ;
	    add ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    mov esi,1           ;
	    add ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    mov esi,3           ;
	    sub ecx,esi         ;
	    mov edx, 1
	    int 0x80
	mov	eax,4       ;
	    mov esi,4           ;
	    add ecx,esi         ;
	    mov edx, 1
	    int 0x80

~~~~

https://etherpad.openstack.org/p/GA-14