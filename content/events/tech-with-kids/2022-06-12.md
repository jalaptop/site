---
startdate:  2022-06-12
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech with kids"
title: "Tech with kids (NL-FR-EN-...)"
price: "Free"
image: "tech_with_kids"
series: "Tech With Kids"
eventtype: "for kids age 6+ with parents"
location: "HSBXL"
---

Placeholder - doing fun stuff with hackery kids. Save the date.
